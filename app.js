const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const http = require('http').Server(app);
const io = require('socket.io')(http);
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const fileUpload = require('express-fileupload');


//rutas
const cursoRoutes = require('./routes/curso');
const colegioRoutes = require('./routes/colegio');
const apoderadoRoutes = require('./routes/apoderado');
const alumnoRoutes = require('./routes/alumno');

app.use(bodyParser.urlencoded({
    limit: '50mb', extended: false ,parameterLimit: 1000000}));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cors({ credentials: true, origin: true }))


//ruta base
// default options
app.use(fileUpload());
app.use('/api', cursoRoutes);
app.use('/api', colegioRoutes);
app.use('/api', apoderadoRoutes);
app.use('/api', alumnoRoutes);



mongoose.connect(config.dbConn)
    .then(() => console.log('go mongo'))
    .catch(err => console.error(err));
    



io.on('connection', (socket) => {
    console.log('user connected');

    socket.on('disconnect', function () {
        console.log('user disconnected');
    });

    socket.on('add-message', (message) => {
        
        io.emit('message', { type: 'new-message', text: message });
    });
    
})

app.listen(3000, () => {
    console.log('MONGODB listening on 3000')
})

http.listen(5000, () => {
    console.log('Server started on port 5000');
});


