'use strict';

var Apoderado = require('../models/apoderado');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('../services/jwt');


function saveApoderado(req, res) {

    var apoderado = new Apoderado();
    var params = req.body;
    apoderado.rut = params.rut;
    apoderado.nombre = params.nombre;
    apoderado.apellidos = params.apellidos;
    apoderado.telefono = params.telefono;
    apoderado.direccion = params.direccion;    
    apoderado.colegio = params.colegio;
    apoderado.perfil = params.perfil;          
        
            if (
                apoderado.nombre!= null
                && apoderado.apellidos != null
                && apoderado.telefono != null
                && apoderado.direccion != null
                && params.password != null
                && apoderado.rut != null
                && apoderado.colegio != null                
                && apoderado.perfil != null                
            ) {
                console.log(apoderado);
                bcrypt.hash(params.password, null, null, function (err, hash) {
                apoderado.password = hash;
                apoderado.save((err, apoderadoStored) => {
                        if (err) {
                            res.status(500).send({ message: 'Error al guardar apoderado' });
                        } else {
                            if (!apoderadoStored) {
                                res.status(404).send({ message: 'No se guardo el apoderado' });
                            } else {
                                res.status(200).send(apoderadoStored);
                            }
                        }
                    });
                });                   
            }else{
                res.status(200).send('Faltan datos que ingresar');
            }
    
    
}

    function loginUser(req, res){        
            var params = req.body;
            var rut = params.rut;
            var password = params.password;

            Apoderado.findOne({ rut: rut }, (err, usuarioData) => {
                if (err) {
                    res.status(500).send({ message: 'Error en el servidor' });
                } else {
                    if (!usuarioData) {
                        res.status(404).send({ message: 'El usuario no existe' });
                        console.log(usuarioData);
                    } else {
                        bcrypt.compare(password, usuarioData.password, function (err, check) {
                            if (check) {                                
                                res.status(200).send({
                                    data:usuarioData,
                                    token: jwt.createToken()
                                });
                            } else {
                                res.status(400).send({ message: 'Contraseña incorrecta' })
                            }
                        });
                    }
                }
            });            
    }

module.exports = {
    saveApoderado,
    loginUser
}