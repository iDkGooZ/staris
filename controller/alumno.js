'use strict';

var Alumno = require('../models/alumno');


function saveAlumno(req, res) {

    var alumno = new Alumno();
    var params = req.body;
    alumno.nombre = params.nombre;
    alumno.fechaNacimiento = params.fechaNacimiento;
    alumno.apellidos = params.apellidos;
    alumno.rut = params.rut;
    alumno.idApoderado = params.idApoderado;
    alumno.telefono = params.telefono;
    alumno.curso = params.curso;
    alumno.idColegio = params.idColegio;

    alumno.save((err, alumnoStored) => {
        if (err) {
            res.status(500).send({ message: 'Error al guardar colegio' });
        } else {
            if (!alumnoStored) {
                res.status(404).send({ message: 'No se guardo el colegio' });
            } else {
                res.status(200).send(alumnoStored);
            }
        }
    })
}

module.exports = {
    saveAlumno
}