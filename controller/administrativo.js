'use strict';

var Administrativo = require('../models/administrativo');


function saveAdministrativo(req, res) {

    var administrativo = new Administrativo();
    var params = req.body;
    administrativo.rut = params.rut;
    administrativo.nombre = params.nombre;
    administrativo.apellidos = params.apellidos;
    administrativo.telefono = params.telefono;
    administrativo.direccion = params.direccion;
    administrativo.password = params.password;
    administrativo.colegio = params.colegio;
    administrativo.perfil = params.perfil;
    
    administrativo.save((err, apoderadoStored) => {
        if (err) {
            res.status(500).send({ message: 'Error al guardar apoderado' });
        } else {
            if (!apoderadoStored) {
                res.status(404).send({ message: 'No se guardo el apoderado' });
            } else {
                res.status(200).send(apoderadoStored);
            }
        }
    })
}

module.exports = {
    saveApoderado
}