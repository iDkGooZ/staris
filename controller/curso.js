'use strict';

var Curso = require('../models/curso');


function saveCurso(req, res){

    var curso = new Curso();
    var params = req.body;    
    curso.nivel = params.nivel;
    curso.nombre = params.nombre;
    curso.letra = params.letra;
    console.log(curso);
    curso.save((err, cursoStored)=>{
        if(err){
            res.status(500).send({ message: 'Error al guardar curso' });
        }else{
            if (!cursoStored){
                res.status(404).send({ message: 'No se guardo el curso' });
            }else{
                res.status(200).send(cursoStored);
            }
        }
    })
}

module.exports = {
    saveCurso
}