'use strict';

var Colegio = require('../models/colegio');


function saveColegio(req, res) {
    
    var colegio = new Colegio();
    var params = req.body;
    colegio.nombre = params.nombre;
    colegio.comuna = params.comuna;
    colegio.direccion = params.direccion;
    colegio.telefono = params.telefono; 
    colegio.img.data = null;
    colegio.img.contentType = null;
    colegio.img.name = null;
    const fileName = 'logoColegio' + '-' + Date.now() + '-' + '.jpg'; 
    if (req.files){
        colegio.img.data = req.files.logo.data;
        colegio.img.contentType = req.files.logo.mimetype;
        colegio.img.name = fileName;
    }
    console.log(req.files.logo)         
        let sampleFile = req.files.logo;
    sampleFile.mv('/Users/banchilemoviles/Documents/app-server/book/uploads/' + fileName, function (err) {
            if (err) {
                res.status(404).send({ message: 'Error al subir imagen' });
            } else {
                colegio.save((err, colegioStored) => {
                    if (err) {
                        res.status(500).send({ message: 'Error al guardar colegio' });
                    } else {
                        if (!colegioStored) {
                            res.status(404).send({ message: 'No se guardo el colegio' });
                        } else {
                            res.status(200).send(colegioStored);
                        }
                    }
                });
            }
        });              
        
            
    }


module.exports = {
    saveColegio
}