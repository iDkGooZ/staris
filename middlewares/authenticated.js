'use strict'
var jwt = require('jwt-simple');
var secret = 'dfe530tx1.,$2426';
var moment = require('moment');




exports.ensureAuth = function(req, res, next){

    if(!req.headers.authorization){
        return res.status(403).send({message : 'La petición no tiene la cabezera de autenticación'})
    }

    var token = req.headers.authorization.replace(/['"]+/g,'');

    try{

        var payload = jwt.decode(token, secret);
        if(payload.exp <= moment().unix()){
            return res.status(403).send({ message: 'Token expirado' })
        }

    }catch(ex){
        return res.status(404).send({ message: 'Token no válido' })
    }

    req.user = payload;

    next();
}