'use strict'
var express = require('express');
var ColegioController = require('../controller/colegio');
var api = express.Router();

api.post('/colegio',ColegioController.saveColegio);


//api.put('/tickets/:id', TicketController.updateTicket);
//api.delete('/tickets/:id', TicketController.deleteTicket);
//api.get('/tickets/:id', TicketController.getTicket);
//api.get('/tickets', TicketController.allTicket);
//api.get('/_verTicketUsuario/:id', TicketController.getTicketUsuario);
//api.get('/_verTicketImage/:imageFile', TicketController.getImageTicket);


module.exports = api;
