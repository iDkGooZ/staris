'use strict'

var express = require('express');
var ApoderadoController = require('../controller/apoderado');
var md_auth = require('../middlewares/authenticated')



var api = express.Router();

//md_auth.ensureAuth
api.post('/apoderado', ApoderadoController.saveApoderado);
api.post('/_login', ApoderadoController.loginUser);
//api.put('/tickets/:id', TicketController.updateTicket);
//api.delete('/tickets/:id', TicketController.deleteTicket);
//api.get('/tickets/:id', TicketController.getTicket);
//api.get('/tickets', TicketController.allTicket);
//api.get('/_verTicketUsuario/:id', TicketController.getTicketUsuario);
//api.get('/_verTicketImage/:imageFile', TicketController.getImageTicket);


module.exports = api;
