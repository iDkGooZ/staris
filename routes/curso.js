'use strict'

var express = require('express');
var CursoController = require('../controller/curso');



var api = express.Router();


api.post('/cursos', CursoController.saveCurso);
//api.put('/tickets/:id', TicketController.updateTicket);
//api.delete('/tickets/:id', TicketController.deleteTicket);
//api.get('/tickets/:id', TicketController.getTicket);
//api.get('/tickets', TicketController.allTicket);
//api.get('/_verTicketUsuario/:id', TicketController.getTicketUsuario);
//api.get('/_verTicketImage/:imageFile', TicketController.getImageTicket);


module.exports = api;
