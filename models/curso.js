'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CursoSchema = Schema({
    nivel:Number,
    nombre:String,
    letra:String
});

module.exports = mongoose.model('Cursos', CursoSchema);