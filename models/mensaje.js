'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var mensaje = Schema({
    tipo: String,
    texto: String,
    fechaHora: { type: Date, default: Date.now },
    rutAlumno: String,
    rutAdministrativo:String,
    documento:String
});

module.exports = mongoose.model('Mensajes', ComunicacionSchema);