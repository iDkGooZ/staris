'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//3 perfiles docentes, administrador, apoderado, directiva
var AdministrativoSchema = Schema({
    rut: String,
    nombre: String,
    apellidos: {paterno:String,materno:String},
    telefono: Number,    
    direccion: String,
    perfil:Number,
    password: String,
    colegio:[{nombre: String,idColegio:String}]
})

module.exports = mongoose.model('Administrativos', AdministrativoSchema);