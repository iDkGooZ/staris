'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AlumnoSchema = Schema({
    nombre: String,
    fechaNacimiento: String,
    apellidos: {materno:String,paterno:String},
    rut: String,
    idApoderado: { type: Schema.ObjectId, ref: 'Apoderado' },
    telefono: Number,
    curso: { type: Schema.ObjectId, ref: 'Cursos' },
    idColegio: { type: Schema.ObjectId, ref: 'Colegios' },
});

module.exports = mongoose.model('Alumnos', AlumnoSchema);