'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ColegioSchema = Schema({
    nombre: String,
    comuna: String,
    direccion: String,
    telefono: Number,
    img: { data: Buffer, contentType: String, name:String }  
});

module.exports = mongoose.model('Colegios', ColegioSchema);