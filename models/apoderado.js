'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ApoderadoSchema = Schema({
    rut: String,
    nombre: String,
    apellidos: {materno:String, paterno:String},
    telefono: Number,    
    direccion: String,
    perfil:Number,
    password:String,
    colegio: [{idColegio:String}]
})

module.exports = mongoose.model('Apoderados', ApoderadoSchema);