'use strict'
var jwt = require('jwt-simple');
var secret = 'dfe530tx1.,$2426';
var moment = require('moment');




exports.createToken = function(user){
    var payload = {      
        iax: moment().unix(),
        exp: moment().add(30 ,'day').unix
    };

    return jwt.encode(payload, secret);
};
